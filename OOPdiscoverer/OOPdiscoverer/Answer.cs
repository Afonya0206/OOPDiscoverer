﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OOPdiscoverer
{
    class Answer
    {
        string text_answer;
        bool is_right;

        public Answer(string text, bool isright)
        {
            text_answer = text;
            is_right = isright;
        }

        public string TextAnswer
        {
            get
            {
                return text_answer;
            }
        }

        public bool IsRight
        {
            get
            {
                return is_right;
            }
        }
    }
}
