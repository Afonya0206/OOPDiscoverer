﻿namespace MyTopic
{
    partial class SlideShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SlideShow));
            this.btnPrewImage = new System.Windows.Forms.Button();
            this.btnNextImage = new System.Windows.Forms.Button();
            this.btnToEnd = new System.Windows.Forms.Button();
            this.btnToBegin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbiCurrentImage = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbCountListImage = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoToTasks = new System.Windows.Forms.Button();
            this.ImageForTopic = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageForTopic)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrewImage
            // 
            this.btnPrewImage.BackgroundImage = global::OOPdiscoverer.Properties.Resources.player_fast_rewind1;
            this.btnPrewImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPrewImage.FlatAppearance.BorderSize = 0;
            this.btnPrewImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrewImage.Location = new System.Drawing.Point(151, 14);
            this.btnPrewImage.Name = "btnPrewImage";
            this.btnPrewImage.Size = new System.Drawing.Size(90, 81);
            this.btnPrewImage.TabIndex = 9;
            this.btnPrewImage.UseVisualStyleBackColor = false;
            this.btnPrewImage.Click += new System.EventHandler(this.btnPrewImage1_Click);
            // 
            // btnNextImage
            // 
            this.btnNextImage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnNextImage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNextImage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNextImage.BackgroundImage")));
            this.btnNextImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNextImage.FlatAppearance.BorderSize = 0;
            this.btnNextImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextImage.Location = new System.Drawing.Point(794, 14);
            this.btnNextImage.Name = "btnNextImage";
            this.btnNextImage.Size = new System.Drawing.Size(90, 81);
            this.btnNextImage.TabIndex = 10;
            this.btnNextImage.UseVisualStyleBackColor = false;
            this.btnNextImage.Click += new System.EventHandler(this.btnNextImage_Click_1);
            // 
            // btnToEnd
            // 
            this.btnToEnd.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnToEnd.BackgroundImage = global::OOPdiscoverer.Properties.Resources.player_next_track;
            this.btnToEnd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnToEnd.FlatAppearance.BorderSize = 0;
            this.btnToEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToEnd.Location = new System.Drawing.Point(935, 14);
            this.btnToEnd.Name = "btnToEnd";
            this.btnToEnd.Size = new System.Drawing.Size(90, 81);
            this.btnToEnd.TabIndex = 11;
            this.btnToEnd.UseVisualStyleBackColor = false;
            this.btnToEnd.Click += new System.EventHandler(this.btnToEnd_Click_1);
            // 
            // btnToBegin
            // 
            this.btnToBegin.BackgroundImage = global::OOPdiscoverer.Properties.Resources.player_previous_track;
            this.btnToBegin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnToBegin.FlatAppearance.BorderSize = 0;
            this.btnToBegin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToBegin.Location = new System.Drawing.Point(8, 14);
            this.btnToBegin.Name = "btnToBegin";
            this.btnToBegin.Size = new System.Drawing.Size(90, 81);
            this.btnToBegin.TabIndex = 12;
            this.btnToBegin.UseVisualStyleBackColor = false;
            this.btnToBegin.Click += new System.EventHandler(this.btnToBegin_Click_1);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(430, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Страница";
            // 
            // lbiCurrentImage
            // 
            this.lbiCurrentImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbiCurrentImage.AutoSize = true;
            this.lbiCurrentImage.Location = new System.Drawing.Point(509, 7);
            this.lbiCurrentImage.Name = "lbiCurrentImage";
            this.lbiCurrentImage.Size = new System.Drawing.Size(19, 16);
            this.lbiCurrentImage.TabIndex = 6;
            this.lbiCurrentImage.Text = "l2";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(534, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "из";
            // 
            // lbCountListImage
            // 
            this.lbCountListImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbCountListImage.AutoSize = true;
            this.lbCountListImage.Location = new System.Drawing.Point(563, 7);
            this.lbCountListImage.Name = "lbCountListImage";
            this.lbCountListImage.Size = new System.Drawing.Size(46, 16);
            this.lbCountListImage.TabIndex = 8;
            this.lbCountListImage.Text = "label4";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnGoToTasks);
            this.panel2.Controls.Add(this.lbCountListImage);
            this.panel2.Controls.Add(this.btnToEnd);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnPrewImage);
            this.panel2.Controls.Add(this.lbiCurrentImage);
            this.panel2.Controls.Add(this.btnNextImage);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnToBegin);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 402);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1035, 107);
            this.panel2.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(445, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Перейти к заданиям";
            // 
            // btnGoToTasks
            // 
            this.btnGoToTasks.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGoToTasks.BackgroundImage = global::OOPdiscoverer.Properties.Resources.player_play;
            this.btnGoToTasks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnGoToTasks.FlatAppearance.BorderSize = 0;
            this.btnGoToTasks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToTasks.Location = new System.Drawing.Point(477, 26);
            this.btnGoToTasks.Name = "btnGoToTasks";
            this.btnGoToTasks.Size = new System.Drawing.Size(80, 62);
            this.btnGoToTasks.TabIndex = 13;
            this.btnGoToTasks.UseVisualStyleBackColor = false;
            this.btnGoToTasks.Click += new System.EventHandler(this.btnGoToTasks_Click);
            // 
            // ImageForTopic
            // 
            this.ImageForTopic.BackColor = System.Drawing.Color.LightCyan;
            this.ImageForTopic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageForTopic.Location = new System.Drawing.Point(0, 0);
            this.ImageForTopic.Name = "ImageForTopic";
            this.ImageForTopic.Size = new System.Drawing.Size(1035, 402);
            this.ImageForTopic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageForTopic.TabIndex = 12;
            this.ImageForTopic.TabStop = false;
            // 
            // SlideShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(1035, 509);
            this.Controls.Add(this.ImageForTopic);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SlideShow";
            this.Text = "Тема";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SlideShow_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageForTopic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrewImage;
        private System.Windows.Forms.Button btnNextImage;
        private System.Windows.Forms.Button btnToEnd;
        private System.Windows.Forms.Button btnToBegin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbiCurrentImage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbCountListImage;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox ImageForTopic;
        private System.Windows.Forms.Button btnGoToTasks;
        private System.Windows.Forms.Label label2;


    }
}

