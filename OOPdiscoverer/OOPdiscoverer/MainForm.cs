﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyTopic;

namespace OOPdiscoverer
{
    public partial class MainForm : Form
    {
        Form CurrentForm = new Form();

        public MainForm()
        {
            InitializeComponent();
        }

        private void компиляторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentForm.Close();
            CompilerForm Compiler = new CompilerForm();
            Compiler.ShowDialog();
        }

        private void тестированиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentForm.Close();
            StartTestingForm StartTesting = new StartTestingForm();
            if (StartTesting.ShowDialog() == DialogResult.OK)
            {
                TestingForm Testing = new TestingForm(StartTesting.CountQuestions, StartTesting.Surname);
                Testing.ShowDialog();
            }
        }

        private void результатыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentForm.Close();
            GeneralResultForm generalResult = new GeneralResultForm();
            generalResult.ShowDialog();
        }

        private void введениевООПToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTopic(введениевООПToolStripMenuItem.Text);
        }

        private void абстракцияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTopic(абстракцияToolStripMenuItem.Text);
        }

        public void ShowTopic(string name)
        {
            try
            {
                CurrentForm.Close();
                SlideShow slide_show = new SlideShow(name, this);
                slide_show.TopLevel = false;
                slide_show.Parent = panelMain;
                slide_show.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                slide_show.Dock = DockStyle.Fill;
                slide_show.Show();
                CurrentForm = slide_show;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ShowChild(Form child)
        {
            CurrentForm.Close();
            child.TopLevel = false;
            child.Parent = panelMain;
            child.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            child.Dock = DockStyle.Fill;
            child.Show();
            CurrentForm = child;
        }

        private void инкапсуляцияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTopic(инкапсуляцияToolStripMenuItem.Text);
        }

        private void наследованиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTopic(наследованиеToolStripMenuItem.Text);
        }

        private void полиморфизмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowTopic(полиморфизмToolStripMenuItem.Text);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ShowTopic(введениевООПToolStripMenuItem.Text);
        }
    }
}
