﻿namespace OOPdiscoverer
{
    partial class ResultTestingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEnd = new System.Windows.Forms.Label();
            this.labelSave = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelAll = new System.Windows.Forms.Label();
            this.labelRight = new System.Windows.Forms.Label();
            this.labelAssessment = new System.Windows.Forms.Label();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.groupBoxResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelEnd
            // 
            this.labelEnd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelEnd.AutoSize = true;
            this.labelEnd.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEnd.Location = new System.Drawing.Point(34, 25);
            this.labelEnd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEnd.Name = "labelEnd";
            this.labelEnd.Size = new System.Drawing.Size(251, 16);
            this.labelEnd.TabIndex = 0;
            this.labelEnd.Text = "Прохождение теста завершено!";
            // 
            // labelSave
            // 
            this.labelSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSave.AutoSize = true;
            this.labelSave.Location = new System.Drawing.Point(44, 200);
            this.labelSave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSave.Name = "labelSave";
            this.labelSave.Size = new System.Drawing.Size(231, 16);
            this.labelSave.TabIndex = 1;
            this.labelSave.Text = "Результаты успешно сохранены!";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonOK.Location = new System.Drawing.Point(105, 234);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 28);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "ОК";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelAll
            // 
            this.labelAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelAll.AutoSize = true;
            this.labelAll.Location = new System.Drawing.Point(15, 52);
            this.labelAll.Name = "labelAll";
            this.labelAll.Size = new System.Drawing.Size(116, 16);
            this.labelAll.TabIndex = 3;
            this.labelAll.Text = "Всего ответов: ";
            // 
            // labelRight
            // 
            this.labelRight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelRight.AutoSize = true;
            this.labelRight.Location = new System.Drawing.Point(15, 78);
            this.labelRight.Name = "labelRight";
            this.labelRight.Size = new System.Drawing.Size(147, 16);
            this.labelRight.TabIndex = 4;
            this.labelRight.Text = "Из них правильных: ";
            // 
            // labelAssessment
            // 
            this.labelAssessment.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelAssessment.AutoSize = true;
            this.labelAssessment.Location = new System.Drawing.Point(15, 104);
            this.labelAssessment.Name = "labelAssessment";
            this.labelAssessment.Size = new System.Drawing.Size(70, 16);
            this.labelAssessment.TabIndex = 5;
            this.labelAssessment.Text = "Оценка: ";
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBoxResults.Controls.Add(this.labelSurname);
            this.groupBoxResults.Controls.Add(this.labelAll);
            this.groupBoxResults.Controls.Add(this.labelAssessment);
            this.groupBoxResults.Controls.Add(this.labelRight);
            this.groupBoxResults.Location = new System.Drawing.Point(41, 57);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(237, 136);
            this.groupBoxResults.TabIndex = 6;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Text = "Результаты";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(15, 26);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(49, 16);
            this.labelSurname.TabIndex = 6;
            this.labelSurname.Text = "ФИО: ";
            // 
            // ResultTestingForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 280);
            this.Controls.Add(this.groupBoxResults);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelSave);
            this.Controls.Add(this.labelEnd);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ResultTestingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Результаты";
            this.groupBoxResults.ResumeLayout(false);
            this.groupBoxResults.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEnd;
        private System.Windows.Forms.Label labelSave;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelAll;
        private System.Windows.Forms.Label labelRight;
        private System.Windows.Forms.Label labelAssessment;
        private System.Windows.Forms.GroupBox groupBoxResults;
        private System.Windows.Forms.Label labelSurname;
    }
}