﻿namespace OOPdiscoverer
{
    partial class StartTestingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNumberQuestions = new System.Windows.Forms.Label();
            this.numUpDnNumberQuestions = new System.Windows.Forms.NumericUpDown();
            this.buttonStart = new System.Windows.Forms.Button();
            this.panelStart = new System.Windows.Forms.Panel();
            this.groupBoxAuthorization = new System.Windows.Forms.GroupBox();
            this.labelSurname = new System.Windows.Forms.Label();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnNumberQuestions)).BeginInit();
            this.panelStart.SuspendLayout();
            this.groupBoxAuthorization.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNumberQuestions
            // 
            this.labelNumberQuestions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNumberQuestions.AutoSize = true;
            this.labelNumberQuestions.Location = new System.Drawing.Point(21, 90);
            this.labelNumberQuestions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNumberQuestions.Name = "labelNumberQuestions";
            this.labelNumberQuestions.Size = new System.Drawing.Size(162, 16);
            this.labelNumberQuestions.TabIndex = 0;
            this.labelNumberQuestions.Text = "Количество вопросов:";
            this.labelNumberQuestions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numUpDnNumberQuestions
            // 
            this.numUpDnNumberQuestions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numUpDnNumberQuestions.Location = new System.Drawing.Point(24, 109);
            this.numUpDnNumberQuestions.Margin = new System.Windows.Forms.Padding(4);
            this.numUpDnNumberQuestions.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUpDnNumberQuestions.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUpDnNumberQuestions.Name = "numUpDnNumberQuestions";
            this.numUpDnNumberQuestions.Size = new System.Drawing.Size(88, 23);
            this.numUpDnNumberQuestions.TabIndex = 1;
            this.numUpDnNumberQuestions.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonStart.BackColor = System.Drawing.Color.Transparent;
            this.buttonStart.Location = new System.Drawing.Point(236, 219);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(178, 52);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Начать тестирование";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // panelStart
            // 
            this.panelStart.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelStart.Controls.Add(this.groupBoxAuthorization);
            this.panelStart.Controls.Add(this.pictureBox1);
            this.panelStart.Controls.Add(this.buttonStart);
            this.panelStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelStart.Location = new System.Drawing.Point(0, 0);
            this.panelStart.Name = "panelStart";
            this.panelStart.Size = new System.Drawing.Size(484, 312);
            this.panelStart.TabIndex = 3;
            // 
            // groupBoxAuthorization
            // 
            this.groupBoxAuthorization.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBoxAuthorization.Controls.Add(this.labelSurname);
            this.groupBoxAuthorization.Controls.Add(this.numUpDnNumberQuestions);
            this.groupBoxAuthorization.Controls.Add(this.textBoxSurname);
            this.groupBoxAuthorization.Controls.Add(this.labelNumberQuestions);
            this.groupBoxAuthorization.Location = new System.Drawing.Point(212, 48);
            this.groupBoxAuthorization.Name = "groupBoxAuthorization";
            this.groupBoxAuthorization.Size = new System.Drawing.Size(220, 150);
            this.groupBoxAuthorization.TabIndex = 6;
            this.groupBoxAuthorization.TabStop = false;
            this.groupBoxAuthorization.Text = "Авторизация";
            // 
            // labelSurname
            // 
            this.labelSurname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(21, 26);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(44, 16);
            this.labelSurname.TabIndex = 3;
            this.labelSurname.Text = "ФИО:";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxSurname.Location = new System.Drawing.Point(24, 45);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(178, 23);
            this.textBoxSurname.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::OOPdiscoverer.Properties.Resources.auth;
            this.pictureBox1.Location = new System.Drawing.Point(23, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(159, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // StartTestingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(484, 312);
            this.Controls.Add(this.panelStart);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StartTestingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тестирование";
            this.Load += new System.EventHandler(this.TestingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnNumberQuestions)).EndInit();
            this.panelStart.ResumeLayout(false);
            this.groupBoxAuthorization.ResumeLayout(false);
            this.groupBoxAuthorization.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelNumberQuestions;
        private System.Windows.Forms.NumericUpDown numUpDnNumberQuestions;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Panel panelStart;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.GroupBox groupBoxAuthorization;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}