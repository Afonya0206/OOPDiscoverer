﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Drawing;
using OOPdiscoverer;

namespace MyTopic
{
    public class Topic
    {
        string NameTopic;
        List<byte[]> ListImage;

        public Topic(string nametopic)
        {
            NameTopic = nametopic;
            ListImage = new List<byte[]>();
        }

        public string GetNameTopic
        {
            get
            {
                return NameTopic;
            }
        }

        public void ReadImageFromDB()
        {
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = DataBaseConnection.StringConnection;
            try
            {
                byte[] bbufer;
                DataTable dt = new DataTable("Topics");
                con.Open();
                OleDbCommand cmd = con.CreateCommand();
                cmd.CommandText = String.Format("Select * From Topics Where  NameTopic = '{0}'", NameTopic);
                dt = new DataTable("Topics");
                OleDbDataAdapter ad = new OleDbDataAdapter(cmd);
                ad.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bbufer = (byte[])dt.Rows[i]["Image"];
                    ListImage.Add(bbufer);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        //индексатор
        public byte[] this[int i]
        {
            get
            {
                if (i < 0 || i >= ListImage.Count)
                    throw new Exception("Выход за границы списка с изображениями.");
                else
                    return ListImage[i];
            }
            set
            {
                if (i < 0 || i >= ListImage.Count)
                    throw new Exception("Выход за границы списка с изображениями.");
                else
                    ListImage[i] = value;
            }
        }

        //Свойство возвращает количество изображений
        public int GetCountListImage
        {
            get
            {
                return ListImage.Count;
            }
        }
    }
}
