﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OOPdiscoverer
{
    class Question
    {
        string text_question;
        List<Answer> answers;
        string name_image;

        public Question(string text, List<Answer> ans, string image)
        {
            text_question = text;
            answers = ans;
            name_image = image;
            MixAnswers();
        }

        public string TextQuestion
        {
            get
            {
                return text_question;
            }
        }

        public List<Answer> Answers
        {
            get
            {
                return answers;
            }
        }

        public string NameImage
        {
            get
            {
                return name_image;
            }
        }

        public bool IsRightAnswer(string Answer)
        {
            for (int i = 0; i < Answers.Count; i++)
                if (Answers[i].IsRight && Answers[i].TextAnswer == Answer)
                    return true;

            return false;
        }

        // метод для перемешивания ответов в случайном порядке
        private void MixAnswers()
        {
            if (answers.Count > 1)
            {
                // объект класса Random для генерации случайных чисел
                Random random = new Random();

                // временная переменная для премещения двух элементов
                Answer temp;

                // переменная для хранения случайного числа
                int randomNumber;

                // перемешивание ответов
                for (int i = 0; i < Answers.Count; i++)
                {
                    for (int j = 0; j < Answers.Count; j++)
                    {
                        randomNumber = random.Next(0, Answers.Count - 1);
                        temp = Answers[j];
                        Answers[j] = Answers[randomNumber];
                        Answers[randomNumber] = temp;
                    }
                }
            }
        }
    }
}
