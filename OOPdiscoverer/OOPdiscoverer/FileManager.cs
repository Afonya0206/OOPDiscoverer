﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// пространство имен для работы с файлом
using System.IO;

namespace OOPdiscoverer
{
    // класс для выполнения операций с файлом
    static class FileManager
    {
        // метод для получения содержимого файла
        // (возвращает текст, содержащийся в файле)
        public static string GetTextFile(string FileName)
        {
            StreamReader reader = File.OpenText(FileName);
            string text = reader.ReadToEnd();
            reader.Close();
            return text;
        }

        // метод для записи текста в файл
        public static void SaveToFile(string FileName, string Text)
        {
            StreamWriter writer = File.CreateText(FileName);
            writer.Write(Text);
            writer.Close();
        }
    }
}
