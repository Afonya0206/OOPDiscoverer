﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

// пространства имен для выполнения программы из файла
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Diagnostics;

// пространства имен для работы с базой данных
using System.Data.OleDb;
using System.Data;

namespace OOPdiscoverer
{
    // класс для выполнения операций компилятора
    public static class Compiler
    {
        // свойство для полйчения настроенных параметров выполнения программы
        private static CompilerParameters GetCompilerParameters
        {
            get
            {
                // создание экземпляра класса параметров для компилятора
                CompilerParameters compileparam = new CompilerParameters();

                // параметр GenerateExecutable указывает, созавать ли исполняемый файл 
                compileparam.GenerateExecutable = true;

                // параметр IncludeDebugInformation указывает включать ли сведения об отладке в файл
                compileparam.IncludeDebugInformation = false;

                // установка имени выходного файла
                compileparam.OutputAssembly = "Program.exe";

                // добавление библиотек

                //compileparam.ReferencedAssemblies.Add("System.Linq.dll");
                compileparam.ReferencedAssemblies.Add("System.Drawing.dll");
                compileparam.ReferencedAssemblies.Add("System.Data.dll");

                // предупреждения не рассматривать как ошибки
                compileparam.TreatWarningsAsErrors = false;

                return compileparam;
            }
        }

        // метод для выполнения программы, код которой передается параметром Text 
        // (возвращает список ошибок)
        public static CompilerErrorCollection CompileAndExecute(string Code)
        {
            // создание экземпляра компилятора C# кода
            CSharpCodeProvider code = new CSharpCodeProvider();
            
            // компиляция
            CompilerResults results = code.CompileAssemblyFromSource(GetCompilerParameters, new string[] { Code });

            // если код не содержит ошибок
            if (results.Errors.Count == 0)
            {
                // запуск программы
                ProcessStartInfo Info = new ProcessStartInfo("Program.exe");
                Process.Start(Info);
            }

            return results.Errors;
        }

        public static CompilerErrorCollection CompileWithoutExecute(string Code)
        {
            // создание экземпляра компилятора C# кода
            CSharpCodeProvider code = new CSharpCodeProvider();

            // компиляция
            CompilerResults results = code.CompileAssemblyFromSource(GetCompilerParameters, new string[] { Code });
            
            return results.Errors;
        }

        // метод, который по номеру ошибки возвращает ее описание из базы данных
        public static string GetDescriptionError(string ErrorNumber)
        {
            // указание источника (путь к базе данных)
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = DataBaseConnection.StringConnection;
            
            // описание команды для выбора необходимых строк из таблицы БД
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = String.Format("Select Description From Error Where ErrorCode='{0}'", ErrorNumber);
            cmd.Connection = conn;
            
            // выполнение команды
            conn.Open();
            object description = cmd.ExecuteScalar();
            conn.Close();
            if (description != null)
                return description.ToString();
            else
                return null;
        }
    }
}
