﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOPdiscoverer
{
    public partial class ResultTestingForm : Form
    {
        public ResultTestingForm(int All, int Right, string Surname)
        {
            InitializeComponent();
            labelSurname.Text += Surname;
            labelAll.Text += All.ToString();
            labelRight.Text += Right.ToString();
            labelAssessment.Text += Test.CalculateAssessment(All, Right);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
