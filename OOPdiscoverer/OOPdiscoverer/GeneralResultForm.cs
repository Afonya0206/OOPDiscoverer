﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOPdiscoverer
{
    public partial class GeneralResultForm : Form
    {
        DataTable Results;

        public GeneralResultForm()
        {
            InitializeComponent();
            Results = Test.AllResults();
        }

        private void GeneralResultForm_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridViewResults.DataSource = Results;
                dataGridViewResults.Columns[0].HeaderText = "ФИО";
                dataGridViewResults.Columns[1].HeaderText = "Дата прохождения";
                dataGridViewResults.Columns[2].HeaderText = "Количество вопросов";
                dataGridViewResults.Columns[3].HeaderText = "Количество правильных ответов";
                dataGridViewResults.Columns[4].HeaderText = "Оценка";
                dataGridViewResults.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridViewResults.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridViewResults.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridViewResults.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                ColorizeDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBoxSurname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBoxSurname.Text != "")
                {
                    dataGridViewResults.DataSource = Results;
                    DataView table = new DataView((DataTable)dataGridViewResults.DataSource);
                    table.RowFilter = String.Format("Surname Like '{0}*'", textBoxSurname.Text);
                    dataGridViewResults.DataSource = table.ToTable();
                }
                else
                    dataGridViewResults.DataSource = Results;
                ColorizeDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ColorizeDataGridView()
        {
            try
            {
                for (int i = 0; i < dataGridViewResults.RowCount - 1; i = i + 2)
                {
                    dataGridViewResults.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(219, 239, 251);
                    dataGridViewResults.Rows[i + 1].DefaultCellStyle.BackColor = Color.FromName("White");
                }

                if (dataGridViewResults.RowCount % 2 != 0)
                    dataGridViewResults.Rows[dataGridViewResults.RowCount - 1].DefaultCellStyle.BackColor = Color.FromArgb(219, 239, 251);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridViewResults_Sorted(object sender, EventArgs e)
        {
            ColorizeDataGridView();
        }
    }
}
