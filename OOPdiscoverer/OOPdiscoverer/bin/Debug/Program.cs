using System;
using System.Collections.Generic;
using System.Text;

namespace lr3z2
{
    class Matr
    {
        int[,] IntArray;
        int n;

        // конструктор с параметрами
        public Matr(int n)
        {
            this.n = n;
            this.IntArray = new int [n,n];
        }

        // метод для ввода значений матрицы
        public void input()
        {
            Console.WriteLine("Введите значения матрицы ({0}*{0})", n);
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++ )
                    IntArray[i, j] = int.Parse(Console.ReadLine());
        }

        // метод для вывода матрицы на экран
        public void output()
        {
            Console.WriteLine("Матрица имеет вид:");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write(IntArray[i, j] + "  ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        // метод для вычисления суммы элементов i-го столбца
        public int sumicol(int col)
        {
            col--;
            int sum = 0;
            for (int i = 0; i < n; i++)
                sum += IntArray[i, col];

            return sum;
        }

        // свойство для получения размерности матрицы
        public int N
        {
            get
            {
                return n;
            }
        }

        // свойство для получения количества нулевых элементов в матрице
        public int ColNull
        {
            get
            {
                int k=0;
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        if (IntArray[i, j] == 0)
                            k++;
                return k;
            }
        }

        // свойство для замены элементов главной диагонали на скаляр
        public int Scalar
        {
            set
            {
                for (int i = 0; i < n; i++)
                    IntArray[i, i] = value;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // ввод размерности матрицы
                Console.Write("Введите размерность матрицы: ");
                int n = int.Parse(Console.ReadLine());

                // создание объекта класса Matr
                Matr m = new Matr(n);

                // заполнение матрицы
                m.input();
                Console.WriteLine();

                // вывод матрицы на экран
                m.output();

                // ввод номера столбца для вычисления суммы
                Console.Write("Введите номер столбца для вычисления суммы его элементов: ");
                int col = int.Parse(Console.ReadLine());

                // вычисление суммы элементов введенного столбца
                if (col <= 0 || col > m.N)  // проверка корректности введенного стоблца
                    Console.WriteLine("Столбец {0} не существует в данной матрице.", col);
                else
                    Console.WriteLine("Сумма элементов {0}-го столбца = {1}", col, m.sumicol(col));
                Console.WriteLine();

                // количество нулевых элементов в матрице
                Console.WriteLine("Количество нулевых элементов в матрице = " + m.ColNull);

                // ввод скаляра и установка значений всех элементов главной диагонали равное скаляру 
                Console.Write("\nВведите скаляр: ");
                m.Scalar = int.Parse(Console.ReadLine());

                // вывод преобразованной матрицы
                Console.WriteLine("\nЗНАЧЕНИЯ ЭЛЕМЕНТОВ ГЛАВНОЙ ДИАГОНАЛИ ЗАМЕНЕНЫ НА СКАЛЯР");
                m.output();  
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
