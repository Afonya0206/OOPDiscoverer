﻿namespace MyTopic
{
    partial class FormTasks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxCode = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtQuestion1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtQuestion2 = new System.Windows.Forms.TextBox();
            this.txtQuestion3 = new System.Windows.Forms.TextBox();
            this.txtQuestion4 = new System.Windows.Forms.TextBox();
            this.txtQuestion5 = new System.Windows.Forms.TextBox();
            this.txtQuestion6 = new System.Windows.Forms.TextBox();
            this.txtQuestion7 = new System.Windows.Forms.TextBox();
            this.txtQuestion8 = new System.Windows.Forms.TextBox();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxCode
            // 
            this.richTextBoxCode.Dock = System.Windows.Forms.DockStyle.Left;
            this.richTextBoxCode.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxCode.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBoxCode.Name = "richTextBoxCode";
            this.richTextBoxCode.Size = new System.Drawing.Size(676, 693);
            this.richTextBoxCode.TabIndex = 0;
            this.richTextBoxCode.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(685, 465);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // txtQuestion1
            // 
            this.txtQuestion1.Location = new System.Drawing.Point(685, 484);
            this.txtQuestion1.Name = "txtQuestion1";
            this.txtQuestion1.Size = new System.Drawing.Size(361, 23);
            this.txtQuestion1.TabIndex = 2;
            this.txtQuestion1.TextChanged += new System.EventHandler(this.txtQuestion1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(683, 401);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(683, 335);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(683, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(683, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(683, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(683, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(683, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "label8";
            // 
            // txtQuestion2
            // 
            this.txtQuestion2.Location = new System.Drawing.Point(686, 420);
            this.txtQuestion2.Name = "txtQuestion2";
            this.txtQuestion2.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion2.TabIndex = 10;
            this.txtQuestion2.TextChanged += new System.EventHandler(this.txtQuestion2_TextChanged);
            // 
            // txtQuestion3
            // 
            this.txtQuestion3.Location = new System.Drawing.Point(686, 354);
            this.txtQuestion3.Name = "txtQuestion3";
            this.txtQuestion3.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion3.TabIndex = 11;
            this.txtQuestion3.TextChanged += new System.EventHandler(this.txtQuestion3_TextChanged);
            // 
            // txtQuestion4
            // 
            this.txtQuestion4.Location = new System.Drawing.Point(686, 290);
            this.txtQuestion4.Name = "txtQuestion4";
            this.txtQuestion4.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion4.TabIndex = 12;
            this.txtQuestion4.TextChanged += new System.EventHandler(this.txtQuestion4_TextChanged);
            // 
            // txtQuestion5
            // 
            this.txtQuestion5.Location = new System.Drawing.Point(686, 222);
            this.txtQuestion5.Name = "txtQuestion5";
            this.txtQuestion5.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion5.TabIndex = 13;
            this.txtQuestion5.TextChanged += new System.EventHandler(this.txtQuestion5_TextChanged);
            // 
            // txtQuestion6
            // 
            this.txtQuestion6.Location = new System.Drawing.Point(686, 154);
            this.txtQuestion6.Name = "txtQuestion6";
            this.txtQuestion6.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion6.TabIndex = 14;
            this.txtQuestion6.TextChanged += new System.EventHandler(this.txtQuestion6_TextChanged);
            // 
            // txtQuestion7
            // 
            this.txtQuestion7.Location = new System.Drawing.Point(686, 90);
            this.txtQuestion7.Name = "txtQuestion7";
            this.txtQuestion7.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion7.TabIndex = 15;
            this.txtQuestion7.TextChanged += new System.EventHandler(this.txtQuestion7_TextChanged);
            // 
            // txtQuestion8
            // 
            this.txtQuestion8.Location = new System.Drawing.Point(686, 28);
            this.txtQuestion8.Name = "txtQuestion8";
            this.txtQuestion8.Size = new System.Drawing.Size(358, 23);
            this.txtQuestion8.TabIndex = 16;
            this.txtQuestion8.TextChanged += new System.EventHandler(this.txtQuestion8_TextChanged);
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCheckOut.BackgroundImage = global::OOPdiscoverer.Properties.Resources.Tick;
            this.btnCheckOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCheckOut.FlatAppearance.BorderSize = 0;
            this.btnCheckOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckOut.Location = new System.Drawing.Point(915, 510);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(195, 148);
            this.btnCheckOut.TabIndex = 17;
            this.btnCheckOut.UseVisualStyleBackColor = false;
            this.btnCheckOut.Click += new System.EventHandler(this.btnCheckOut_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::OOPdiscoverer.Properties.Resources._0987b236246d07b0e3130e1603c39489;
            this.pictureBox1.Location = new System.Drawing.Point(683, 512);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(226, 172);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // USE_FormTasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1122, 693);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCheckOut);
            this.Controls.Add(this.txtQuestion8);
            this.Controls.Add(this.txtQuestion7);
            this.Controls.Add(this.txtQuestion6);
            this.Controls.Add(this.txtQuestion5);
            this.Controls.Add(this.txtQuestion4);
            this.Controls.Add(this.txtQuestion3);
            this.Controls.Add(this.txtQuestion2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtQuestion1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBoxCode);
            this.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "USE_FormTasks";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "USE_FormTasks";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTasks_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtQuestion1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtQuestion2;
        private System.Windows.Forms.TextBox txtQuestion3;
        private System.Windows.Forms.TextBox txtQuestion4;
        private System.Windows.Forms.TextBox txtQuestion5;
        private System.Windows.Forms.TextBox txtQuestion6;
        private System.Windows.Forms.TextBox txtQuestion7;
        private System.Windows.Forms.TextBox txtQuestion8;
        private System.Windows.Forms.Button btnCheckOut;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}