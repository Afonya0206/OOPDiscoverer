﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

namespace OOPdiscoverer
{
    public partial class CompilerForm : Form
    {
        string TextCode;
        string[,] NumberErrors;    

        public CompilerForm()
        {
            InitializeComponent();
        }

        private void FormCompiler_Load(object sender, EventArgs e)
        {
            openFileDialog.Title = "Выберите файл для открытия";
            openFileDialog.FileName = "Program";
            openFileDialog.Filter = "Файлы cs|*.cs|Файлы txt|*.txt|Файлы doc|*.doc";

            saveFileDialog.FileName = "Program";
            saveFileDialog.Filter = "Файлы cs|*.cs|Файлы txt|*.txt|Файлы doc|*.doc";

            richTextBoxCode.BackColor = Color.FromArgb(219, 239, 251);
        }

        private void ShowListErrors(CompilerErrorCollection errors)
        {
            UpdateRichTextBox();

            listBoxErrors.Items.Clear();

            // вывод списка ошибок, если они есть
            if (errors.Count > 0)
            {
                NumberErrors = new string[errors.Count, 2];
                for(int i=0; i < errors.Count; i++)
                {
                    string description = Compiler.GetDescriptionError(errors[i].ErrorNumber.ToString());
                    if (description != null)
                        listBoxErrors.Items.Add(String.Format("{0} (ошибка в строке № {1} : {2})", errors[i].ErrorText, errors[i].Line, description, errors[i].ErrorNumber));
                    else
                        listBoxErrors.Items.Add(String.Format("{0} (ошибка в строке № {1})", errors[i].ErrorText, errors[i].Line, errors[i].ErrorNumber));
                    NumberErrors[i, 0] = i.ToString();
                    NumberErrors[i, 1] = errors[i].Line.ToString();
                }
                MessageBox.Show("Код содержит ошибки!");
            }
        }

        // выделение соответствующей строки кода при двойном щелчке по ошибке
        private void listBoxErrors_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (listBoxErrors.SelectedIndex >= 0)
                {
                    int nstr = int.Parse(NumberErrors[listBoxErrors.SelectedIndex, 1]) - 1;
                    int numline = nstr;
                    int i, n, p1, p2;
                    int ln = richTextBoxCode.TextLength;

                    p1 = ln;
                    p2 = ln;
                    for (i = 0; i < ln; i++)
                    {
                        n = richTextBoxCode.GetLineFromCharIndex(i);
                        if ((n == numline) && (p1 == ln)) p1 = i;
                        if (n > numline)
                        {
                            p2 = i;
                            break;
                        }
                    }

                    UpdateRichTextBox();

                    richTextBoxCode.SelectionBackColor = Color.LightCyan;
                    richTextBoxCode.Select(p1, p2 - p1);
                    richTextBoxCode.SelectionBackColor = Color.Red;
                    richTextBoxCode.Select(p1, 0);
                    richTextBoxCode.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        // метод для записи кода текущего программы в поле класса FormCompiler
        private void UpdateRichTextBox()
        {
            TextCode = richTextBoxCode.Text;
            richTextBoxCode.Text = TextCode;
        }

        private void toolStripButtonShowListErrors_Click(object sender, EventArgs e)
        {
            try
            {
                if (toolStripButtonShowListErrors.Text == "Скрыть список ошибок")
                {
                    panelErrors.Visible = false;
                    splitter.Visible = false;
                    toolStripButtonShowListErrors.Text = "Показать список ошибок";
                }
                else
                {
                    panelErrors.Visible = true;
                    splitter.Visible = true;
                    toolStripButtonShowListErrors.Text = "Скрыть список ошибок";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ToolStripMenuItemOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBoxCode.Text = FileManager.GetTextFile(openFileDialog.FileName);
                listBoxErrors.Items.Clear();
            }
        }

        private void ToolStripMenuItemSaveFile_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileManager.SaveToFile(saveFileDialog.FileName, richTextBoxCode.Text);
            }
        }

        private void ToolStripMenuItemCompileWithoutExecute_Click(object sender, EventArgs e)
        {
            try
            {
                listBoxErrors.Items.Clear();
                if (richTextBoxCode.Text == "")
                    MessageBox.Show("Введите код программы или откройте файл для выполнения");
                else
                {
                    // очистка списка ошибок
                    listBoxErrors.Items.Clear();

                    // компиляция программы без выполнения
                    CompilerErrorCollection errors = Compiler.CompileWithoutExecute(richTextBoxCode.Text);

                    // вывод списка ошибок
                    ShowListErrors(errors);

                    // если ошибок нет, вывод сообщения о том, что ошибок нет
                    if (errors.Count == 0)
                        MessageBox.Show("Ошибок нет", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ToolStripMenuItemCompileAndExecute_Click(object sender, EventArgs e)
        {
            try
            {
                listBoxErrors.Items.Clear();
                if (richTextBoxCode.Text == "")
                    MessageBox.Show("Введите код программы или откройте файл для выполнения");
                else
                {
                    CompilerErrorCollection errors = Compiler.CompileAndExecute(richTextBoxCode.Text);
                    ShowListErrors(errors);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
