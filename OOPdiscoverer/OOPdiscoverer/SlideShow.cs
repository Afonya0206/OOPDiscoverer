﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Drawing.Imaging;
using OOPdiscoverer;

namespace MyTopic
{
    public partial class SlideShow : Form
    {
        Topic topic;
        int iCurrentImage;
        MainForm main;

        public SlideShow()
        {
            InitializeComponent();
        }

        public SlideShow(string topicname, MainForm main)
        {
            topic = new Topic(topicname);
            iCurrentImage = 0;
            InitializeComponent();
            this.main = main;
        }

        public FormTasks USE_FormTasks
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        private void SlideShow_Load(object sender, EventArgs e)
        {
            try
            {
                topic.ReadImageFromDB();
                btnPrewbtnToBeginEnable();
                btnNextbtnToEndEnable();
                ImageForTopic.Image = byteArrayToImage(topic[iCurrentImage]);
                lbCountListImage.Text = topic.GetCountListImage.ToString();
                lbiCurrentImage.Text = (iCurrentImage + 1).ToString();
                if (topic.GetNameTopic == "Абстракция" || topic.GetNameTopic == "Инкапсуляция")
                {
                    btnGoToTasks.Visible = false;
                    label2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //метод для преобразования массива byte в изображение
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void btnToBegin_Click_1(object sender, EventArgs e)
        {
            try
            {
                iCurrentImage = 0;
                lbiCurrentImage.Text = (iCurrentImage + 1).ToString();
                btnPrewbtnToBeginEnable();
                btnNextbtnToEndEnable();
                ImageForTopic.Image = byteArrayToImage(topic[iCurrentImage]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnToEnd_Click_1(object sender, EventArgs e)
        {
            try
            {
                iCurrentImage = topic.GetCountListImage - 1;
                lbiCurrentImage.Text = topic.GetCountListImage.ToString();
                btnPrewbtnToBeginEnable();
                btnNextbtnToEndEnable();
                ImageForTopic.Image = byteArrayToImage(topic[iCurrentImage]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPrewImage1_Click(object sender, EventArgs e)
        {
            try
            {
                iCurrentImage--;
                lbiCurrentImage.Text = (iCurrentImage + 1).ToString();
                btnPrewbtnToBeginEnable();
                btnNextbtnToEndEnable();
                ImageForTopic.Image = byteArrayToImage(topic[iCurrentImage]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNextImage_Click_1(object sender, EventArgs e)
        {
            try
            {
                iCurrentImage++;
                lbiCurrentImage.Text = (iCurrentImage + 1).ToString();
                btnNextbtnToEndEnable();
                btnPrewbtnToBeginEnable();
                ImageForTopic.Image = byteArrayToImage(topic[iCurrentImage]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPrewbtnToBeginEnable()
        {
            if (iCurrentImage == 0)
            {
                btnPrewImage.Enabled = false;
                btnToBegin.Enabled = false;
            }
            else
            {
                btnPrewImage.Enabled = true;
                btnToBegin.Enabled = true;
            }
        }

        private void btnNextbtnToEndEnable()
        {
            if (iCurrentImage == topic.GetCountListImage-1)
            {
                btnNextImage.Enabled = false;
                btnToEnd.Enabled = false;
            }
            else
            {
                btnNextImage.Enabled = true;
                btnToEnd.Enabled = true;
            }
        }

        private void btnGoToTasks_Click(object sender, EventArgs e)
        {
            FormTasks FT = new FormTasks(topic.GetNameTopic);
            main.ShowChild(FT);
        }
    } 
}
