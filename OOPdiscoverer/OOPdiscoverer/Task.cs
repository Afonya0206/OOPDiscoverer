﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using OOPdiscoverer;

namespace MyTopic
{
    class Task
    {
        string NameTopic;
        List<SubTask> MasSubTasks;

        public Task(string topicname)
        {
            NameTopic = topicname;
            MasSubTasks = new List<SubTask>();
        }

        //метод для заполнения формы кодом
        public string GetCodeFromFile()
        {
            string Code = (string)OOPdiscoverer.Properties.Resources.ResourceManager.GetObject(NameTopic.Replace(" ","_"));
            return Code;
        }

        //Свойство возвращает количество пар Вопрос-Ответ
        public int GetCountMasSubTasks
        {
            get
            {
                return MasSubTasks.Count;
            }
        }

        //индексатор
        public SubTask this[int i]
        {
            get
            {
                if (i < 0 || i >= MasSubTasks.Count)
                    throw new Exception("Выход за границы списка подзаданий.");
                else
                    return MasSubTasks[i];
            }
            set
            {
                if (i < 0 || i >= MasSubTasks.Count)
                    throw new Exception("Выход за границы списка подзаданий.");
                else
                    MasSubTasks[i] = value;
            }
        }

        //метод для считывания вопросов и ответов
        public void GetQuestionAnswer()
        {
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = DataBaseConnection.StringConnection;
            try
            {
                DataTable dt = new DataTable("QuestionAnswer");
                con.Open();
                OleDbCommand cmd = new OleDbCommand(String.Format("SELECT * FROM QuestionAnswer WHERE NameTopic = '{0}' ORDER BY id", NameTopic), con);
                OleDbDataAdapter ad = new OleDbDataAdapter(cmd);
                ad.Fill(dt);
                SubTask tmpSubTask; 
                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    tmpSubTask = new SubTask();
                    tmpSubTask.QUESTION = dt.Rows[i]["Question"].ToString();
                    tmpSubTask.ANSWER = dt.Rows[i]["Answer"].ToString();
                    MasSubTasks.Add(tmpSubTask);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
