﻿namespace OOPdiscoverer
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.обучениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.введениевООПToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.принципыООПToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.абстракцияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инкапсуляцияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.наследованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.полиморфизмToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.компиляторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тестированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.результатыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelMain = new System.Windows.Forms.Panel();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.BackColor = System.Drawing.Color.LightCyan;
            this.MainMenu.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обучениеToolStripMenuItem,
            this.компиляторToolStripMenuItem,
            this.тестированиеToolStripMenuItem,
            this.результатыToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.MainMenu.Size = new System.Drawing.Size(1030, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip1";
            // 
            // обучениеToolStripMenuItem
            // 
            this.обучениеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.введениевООПToolStripMenuItem,
            this.принципыООПToolStripMenuItem});
            this.обучениеToolStripMenuItem.Name = "обучениеToolStripMenuItem";
            this.обучениеToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.обучениеToolStripMenuItem.Text = "Обучение";
            // 
            // введениевООПToolStripMenuItem
            // 
            this.введениевООПToolStripMenuItem.Name = "введениевООПToolStripMenuItem";
            this.введениевООПToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.введениевООПToolStripMenuItem.Text = "Введение в ООП";
            this.введениевООПToolStripMenuItem.Click += new System.EventHandler(this.введениевООПToolStripMenuItem_Click);
            // 
            // принципыООПToolStripMenuItem
            // 
            this.принципыООПToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.абстракцияToolStripMenuItem,
            this.инкапсуляцияToolStripMenuItem,
            this.наследованиеToolStripMenuItem,
            this.полиморфизмToolStripMenuItem});
            this.принципыООПToolStripMenuItem.Name = "принципыООПToolStripMenuItem";
            this.принципыООПToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.принципыООПToolStripMenuItem.Text = "Принципы ООП";
            // 
            // абстракцияToolStripMenuItem
            // 
            this.абстракцияToolStripMenuItem.Name = "абстракцияToolStripMenuItem";
            this.абстракцияToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.абстракцияToolStripMenuItem.Text = "Абстракция";
            this.абстракцияToolStripMenuItem.Click += new System.EventHandler(this.абстракцияToolStripMenuItem_Click);
            // 
            // инкапсуляцияToolStripMenuItem
            // 
            this.инкапсуляцияToolStripMenuItem.Name = "инкапсуляцияToolStripMenuItem";
            this.инкапсуляцияToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.инкапсуляцияToolStripMenuItem.Text = "Инкапсуляция";
            this.инкапсуляцияToolStripMenuItem.Click += new System.EventHandler(this.инкапсуляцияToolStripMenuItem_Click);
            // 
            // наследованиеToolStripMenuItem
            // 
            this.наследованиеToolStripMenuItem.Name = "наследованиеToolStripMenuItem";
            this.наследованиеToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.наследованиеToolStripMenuItem.Text = "Наследование";
            this.наследованиеToolStripMenuItem.Click += new System.EventHandler(this.наследованиеToolStripMenuItem_Click);
            // 
            // полиморфизмToolStripMenuItem
            // 
            this.полиморфизмToolStripMenuItem.Name = "полиморфизмToolStripMenuItem";
            this.полиморфизмToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.полиморфизмToolStripMenuItem.Text = "Полиморфизм";
            this.полиморфизмToolStripMenuItem.Click += new System.EventHandler(this.полиморфизмToolStripMenuItem_Click);
            // 
            // компиляторToolStripMenuItem
            // 
            this.компиляторToolStripMenuItem.Name = "компиляторToolStripMenuItem";
            this.компиляторToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.компиляторToolStripMenuItem.Text = "Компилятор";
            this.компиляторToolStripMenuItem.Click += new System.EventHandler(this.компиляторToolStripMenuItem_Click);
            // 
            // тестированиеToolStripMenuItem
            // 
            this.тестированиеToolStripMenuItem.Name = "тестированиеToolStripMenuItem";
            this.тестированиеToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.тестированиеToolStripMenuItem.Text = "Тестирование";
            this.тестированиеToolStripMenuItem.Click += new System.EventHandler(this.тестированиеToolStripMenuItem_Click);
            // 
            // результатыToolStripMenuItem
            // 
            this.результатыToolStripMenuItem.Name = "результатыToolStripMenuItem";
            this.результатыToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.результатыToolStripMenuItem.Text = "Результаты";
            this.результатыToolStripMenuItem.Click += new System.EventHandler(this.результатыToolStripMenuItem_Click);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.LightCyan;
            this.panelMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 24);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1030, 586);
            this.panelMain.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(1030, 610);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainForm";
            this.Text = "OOPdiscoverer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem обучениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem введениевООПToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem принципыООПToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem абстракцияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инкапсуляцияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наследованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem полиморфизмToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem компиляторToolStripMenuItem;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ToolStripMenuItem тестированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem результатыToolStripMenuItem;
    }
}

