﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOPdiscoverer
{
    public partial class StartTestingForm : Form
    {
        public int CountQuestions;
        public string Surname;

        public StartTestingForm()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TestingForm_Load(object sender, EventArgs e)
        {
            try
            {
                Test test = new Test(0);
                numUpDnNumberQuestions.Maximum = test.MaxCountQuestions;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxSurname.Text == string.Empty)
                    MessageBox.Show("Заполните поле ФИО!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    CountQuestions = (int)numUpDnNumberQuestions.Value;
                    Surname = textBoxSurname.Text;
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }            
    }
}
