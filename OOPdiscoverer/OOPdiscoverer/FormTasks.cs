﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyTopic
{
    public partial class FormTasks : Form
    {
        Task task;

        public FormTasks(string nametopic)
        {
            task = new Task(nametopic);
            InitializeComponent();
        }

        private void FormTasks_Load(object sender, EventArgs e)
        {
            try
            {
                InvisibleLabelsTextBoxes();
                richTextBoxCode.Text = task.GetCodeFromFile();
                task.GetQuestionAnswer();
                FillQuestionLabels();
                VisibleTextBoxs();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //метод для заполнения формы вопросами
        private void FillQuestionLabels()
        {
            int i = 0;
            foreach (Control ElementOfForm in this.Controls)
            {
                if (ElementOfForm is Label && i != task.GetCountMasSubTasks)
                {
                    ElementOfForm.Text = task[i].QUESTION;
                    i++;
                }
            }
        }

        //метод для невидимости элементов Label и TextBox
        private void InvisibleLabelsTextBoxes()
        {
            foreach (Control ElementOfForm in this.Controls)
            {
                if (ElementOfForm is Label)
                {
                    ElementOfForm.Text = "";
                }
                if (ElementOfForm is TextBox)
                {
                    ElementOfForm.Visible = false;
                }
            }
        }

        //метод для видимости TextBox
        private void VisibleTextBoxs()
        {
            int i = 0;
            foreach (Control ElementOfForm in this.Controls)
            {
                if (ElementOfForm is TextBox && i != task.GetCountMasSubTasks)
                {
                    ElementOfForm.Visible = true;
                    i++;
                }
            }
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                foreach (Control ElementOfForm in this.Controls)
                {
                    if (ElementOfForm is TextBox && ElementOfForm.Visible)
                    {
                        if(ElementOfForm.Text == "")
                            flag = true;
                    }
                }
                if(flag)
                {
                    MessageBox.Show("Вы ответили не на все вопросы!!!");
                }
                else
                {
                    int i = 0;
                    foreach (Control ElementOfForm in this.Controls)
                    {
                        if (ElementOfForm is TextBox && i != task.GetCountMasSubTasks)
                        {
                            if (ElementOfForm.Text.Trim() == task[i].ANSWER)
                                ElementOfForm.ForeColor = Color.Green;
                            else
                                ElementOfForm.ForeColor = Color.Red;
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtQuestion8_TextChanged(object sender, EventArgs e)
        {
            txtQuestion8.ForeColor = Color.Black;
        }

        private void txtQuestion7_TextChanged(object sender, EventArgs e)
        {
            txtQuestion7.ForeColor = Color.Black;
        }

        private void txtQuestion6_TextChanged(object sender, EventArgs e)
        {
            txtQuestion6.ForeColor = Color.Black;
        }

        private void txtQuestion5_TextChanged(object sender, EventArgs e)
        {
            txtQuestion5.ForeColor = Color.Black;
        }

        private void txtQuestion4_TextChanged(object sender, EventArgs e)
        {
            txtQuestion4.ForeColor = Color.Black;
        }

        private void txtQuestion3_TextChanged(object sender, EventArgs e)
        {
            txtQuestion3.ForeColor = Color.Black;
        }

        private void txtQuestion2_TextChanged(object sender, EventArgs e)
        {
            txtQuestion2.ForeColor = Color.Black;
        }

        private void txtQuestion1_TextChanged(object sender, EventArgs e)
        {
            txtQuestion1.ForeColor = Color.Black;
        }
    }
}
