﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOPdiscoverer
{
    public partial class TestingForm : Form
    {
        // объект класса Test для получения вопросов
        Test test;

        // номер текущего вопроса
        int NCurrentQuestion;

        string Surname;

        int Count;

        int CountRight;

        string CurrentAnswer;

        bool IsEnd;

        public TestingForm(int count, string surname)
        {
            try
            {
                InitializeComponent();
                NCurrentQuestion = -1;
                test = new Test(count);
                Count = count;
                Surname = surname;
                CountRight = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TestingForm_Load(object sender, EventArgs e)
        {
            try
            {
                radioButtonAnswer1.Checked = false;
                radioButtonAnswer2.Checked = false;
                radioButtonAnswer3.Checked = false;
                radioButtonAnswer4.Checked = false;
                buttonNext_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            try
            {
                // проверяем правильность ответа на предыдущий вопрос
                if (NCurrentQuestion != -1 && test[NCurrentQuestion].IsRightAnswer(CurrentAnswer))
                    CountRight++;

                NCurrentQuestion++;
                HideElements();
                Question question = test[NCurrentQuestion];
                if (question == null)
                {
                    pictureBoxImage.Visible = false;
                    groupBoxQuestion.Visible = false;
                    buttonNext.Visible = false;
                    test.WriteResult(Surname, Count, CountRight);
                    ResultTestingForm resultForm = new ResultTestingForm(Count, CountRight, Surname);
                    resultForm.ShowDialog();
                    IsEnd = true;           
                    this.Close();     
                }
                else
                {
                    buttonNext.Enabled = false;
                    labelQuestion.Text = question.TextQuestion;
                    groupBoxQuestion.Text = "Вопрос №" + (NCurrentQuestion + 1) + " из " + Count;
                    if (question.Answers.Count == 1)
                    {
                        textBoxAnswer.Text = "";
                        textBoxAnswer.Visible = true;
                        labelAnswer.Visible = true;
                        textBoxAnswer.Select();
                    }
                    else
                        ShowRadiobuttons(question);

                    if (question.NameImage != string.Empty)
                    {
                        System.Resources.ResourceManager res = OOPdiscoverer.Properties.Resources.ResourceManager;
                        pictureBoxImage.Image = (Image)res.GetObject(question.NameImage);
                    }
                    else
                        pictureBoxImage.Image = OOPdiscoverer.Properties.Resources.Question;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // метод для сокрытия элементов формы
        private void HideElements()
        {
            try
            {
                radioButtonAnswer1.Visible = false;
                radioButtonAnswer2.Visible = false;
                radioButtonAnswer3.Visible = false;
                radioButtonAnswer4.Visible = false;
                radioButtonAnswer1.Checked = false;
                radioButtonAnswer2.Checked = false;
                radioButtonAnswer3.Checked = false;
                radioButtonAnswer4.Checked = false;
                textBoxAnswer.Visible = false;
                labelAnswer.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ShowRadiobuttons(Question t)
        {
            try
            {
                switch (t.Answers.Count)
                {
                    case 2:
                        {
                            radioButtonAnswer1.Visible = true;
                            radioButtonAnswer1.Text = t.Answers[0].TextAnswer;
                            radioButtonAnswer2.Visible = true;
                            radioButtonAnswer2.Text = t.Answers[1].TextAnswer;
                            break;
                        }
                    case 3:
                        {
                            radioButtonAnswer1.Visible = true;
                            radioButtonAnswer1.Text = t.Answers[0].TextAnswer;
                            radioButtonAnswer2.Visible = true;
                            radioButtonAnswer2.Text = t.Answers[1].TextAnswer;
                            radioButtonAnswer3.Visible = true;
                            radioButtonAnswer3.Text = t.Answers[2].TextAnswer;
                            break;
                        }
                    case 4:
                        {
                            radioButtonAnswer1.Visible = true;
                            radioButtonAnswer1.Text = t.Answers[0].TextAnswer;
                            radioButtonAnswer2.Visible = true;
                            radioButtonAnswer2.Text = t.Answers[1].TextAnswer;
                            radioButtonAnswer3.Visible = true;
                            radioButtonAnswer3.Text = t.Answers[2].TextAnswer;
                            radioButtonAnswer4.Visible = true;
                            radioButtonAnswer4.Text = t.Answers[3].TextAnswer;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBoxAnswer_TextChanged(object sender, EventArgs e)
        {
            if (textBoxAnswer.Text == string.Empty)
                buttonNext.Enabled = false;
            else
            {
                CurrentAnswer = textBoxAnswer.Text;
                buttonNext.Enabled = true;
            }
        }

        private void radioButtonAnswer1_CheckedChanged(object sender, EventArgs e)
        {
            CurrentAnswer = radioButtonAnswer1.Text;
            buttonNext.Enabled = true;
        }

        private void radioButtonAnswer2_CheckedChanged(object sender, EventArgs e)
        {
            CurrentAnswer = radioButtonAnswer2.Text;
            buttonNext.Enabled = true;
        }

        private void radioButtonAnswer3_CheckedChanged(object sender, EventArgs e)
        {
            CurrentAnswer = radioButtonAnswer3.Text;
            buttonNext.Enabled = true;
        }

        private void radioButtonAnswer4_CheckedChanged(object sender, EventArgs e)
        {
            CurrentAnswer = radioButtonAnswer4.Text;
            buttonNext.Enabled = true;
        }

        private void TestingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsEnd && MessageBox.Show("Вы уверены, что хотите прервать прохождение теста без сохранения результата?",
                "",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation) 
                == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
