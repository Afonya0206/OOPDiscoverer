﻿namespace OOPdiscoverer
{
    partial class CompilerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelErrors = new System.Windows.Forms.Panel();
            this.listBoxErrors = new System.Windows.Forms.ListBox();
            this.labelErrors = new System.Windows.Forms.Label();
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.splitter = new System.Windows.Forms.Splitter();
            this.richTextBoxCode = new System.Windows.Forms.RichTextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolStripFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripMenuItemOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSaveFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComliling = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToolStripMenuItemCompileWithoutExecute = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemCompileAndExecute = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonShowListErrors = new System.Windows.Forms.ToolStripButton();
            this.panelErrors.SuspendLayout();
            this.toolStripMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelErrors
            // 
            this.panelErrors.Controls.Add(this.listBoxErrors);
            this.panelErrors.Controls.Add(this.labelErrors);
            this.panelErrors.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelErrors.Location = new System.Drawing.Point(0, 434);
            this.panelErrors.Margin = new System.Windows.Forms.Padding(4);
            this.panelErrors.Name = "panelErrors";
            this.panelErrors.Size = new System.Drawing.Size(718, 123);
            this.panelErrors.TabIndex = 0;
            // 
            // listBoxErrors
            // 
            this.listBoxErrors.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.listBoxErrors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxErrors.FormattingEnabled = true;
            this.listBoxErrors.HorizontalScrollbar = true;
            this.listBoxErrors.ItemHeight = 16;
            this.listBoxErrors.Location = new System.Drawing.Point(0, 20);
            this.listBoxErrors.Name = "listBoxErrors";
            this.listBoxErrors.Size = new System.Drawing.Size(718, 103);
            this.listBoxErrors.TabIndex = 1;
            this.listBoxErrors.DoubleClick += new System.EventHandler(this.listBoxErrors_DoubleClick);
            // 
            // labelErrors
            // 
            this.labelErrors.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelErrors.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelErrors.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelErrors.Location = new System.Drawing.Point(0, 0);
            this.labelErrors.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelErrors.Name = "labelErrors";
            this.labelErrors.Size = new System.Drawing.Size(718, 20);
            this.labelErrors.TabIndex = 0;
            this.labelErrors.Text = "Список ошибок";
            this.labelErrors.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFile,
            this.toolStripSeparator1,
            this.toolStripComliling,
            this.toolStripButtonShowListErrors});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripMenu.Size = new System.Drawing.Size(718, 25);
            this.toolStripMenu.TabIndex = 2;
            this.toolStripMenu.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // splitter
            // 
            this.splitter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter.Location = new System.Drawing.Point(0, 428);
            this.splitter.Margin = new System.Windows.Forms.Padding(4);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(718, 6);
            this.splitter.TabIndex = 3;
            this.splitter.TabStop = false;
            // 
            // richTextBoxCode
            // 
            this.richTextBoxCode.AcceptsTab = true;
            this.richTextBoxCode.BackColor = System.Drawing.Color.LightCyan;
            this.richTextBoxCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxCode.Location = new System.Drawing.Point(0, 25);
            this.richTextBoxCode.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBoxCode.Name = "richTextBoxCode";
            this.richTextBoxCode.ShowSelectionMargin = true;
            this.richTextBoxCode.Size = new System.Drawing.Size(718, 403);
            this.richTextBoxCode.TabIndex = 4;
            this.richTextBoxCode.Text = "";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // toolStripFile
            // 
            this.toolStripFile.AutoToolTip = false;
            this.toolStripFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemOpenFile,
            this.ToolStripMenuItemSaveFile});
            this.toolStripFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripFile.Name = "toolStripFile";
            this.toolStripFile.Size = new System.Drawing.Size(56, 22);
            this.toolStripFile.Text = "Файл";
            // 
            // ToolStripMenuItemOpenFile
            // 
            this.ToolStripMenuItemOpenFile.Name = "ToolStripMenuItemOpenFile";
            this.ToolStripMenuItemOpenFile.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItemOpenFile.Text = "Открыть";
            this.ToolStripMenuItemOpenFile.Click += new System.EventHandler(this.ToolStripMenuItemOpenFile_Click);
            // 
            // ToolStripMenuItemSaveFile
            // 
            this.ToolStripMenuItemSaveFile.Name = "ToolStripMenuItemSaveFile";
            this.ToolStripMenuItemSaveFile.Size = new System.Drawing.Size(152, 22);
            this.ToolStripMenuItemSaveFile.Text = "Сохранить";
            this.ToolStripMenuItemSaveFile.Click += new System.EventHandler(this.ToolStripMenuItemSaveFile_Click);
            // 
            // toolStripComliling
            // 
            this.toolStripComliling.AutoToolTip = false;
            this.toolStripComliling.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemCompileWithoutExecute,
            this.ToolStripMenuItemCompileAndExecute});
            this.toolStripComliling.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripComliling.Name = "toolStripComliling";
            this.toolStripComliling.Size = new System.Drawing.Size(119, 22);
            this.toolStripComliling.Text = "Компиляция";
            // 
            // ToolStripMenuItemCompileWithoutExecute
            // 
            this.ToolStripMenuItemCompileWithoutExecute.Name = "ToolStripMenuItemCompileWithoutExecute";
            this.ToolStripMenuItemCompileWithoutExecute.Size = new System.Drawing.Size(293, 22);
            this.ToolStripMenuItemCompileWithoutExecute.Text = "Компилировать без выполнения";
            this.ToolStripMenuItemCompileWithoutExecute.Click += new System.EventHandler(this.ToolStripMenuItemCompileWithoutExecute_Click);
            // 
            // ToolStripMenuItemCompileAndExecute
            // 
            this.ToolStripMenuItemCompileAndExecute.Name = "ToolStripMenuItemCompileAndExecute";
            this.ToolStripMenuItemCompileAndExecute.Size = new System.Drawing.Size(293, 22);
            this.ToolStripMenuItemCompileAndExecute.Text = "Компилировать и выполнить";
            this.ToolStripMenuItemCompileAndExecute.Click += new System.EventHandler(this.ToolStripMenuItemCompileAndExecute_Click);
            // 
            // toolStripButtonShowListErrors
            // 
            this.toolStripButtonShowListErrors.AutoToolTip = false;
            this.toolStripButtonShowListErrors.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowListErrors.Name = "toolStripButtonShowListErrors";
            this.toolStripButtonShowListErrors.Size = new System.Drawing.Size(185, 22);
            this.toolStripButtonShowListErrors.Text = "Скрыть список ошибок";
            this.toolStripButtonShowListErrors.Click += new System.EventHandler(this.toolStripButtonShowListErrors_Click);
            // 
            // CompilerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(718, 557);
            this.Controls.Add(this.richTextBoxCode);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.toolStripMenu);
            this.Controls.Add(this.panelErrors);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CompilerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Компилятор";
            this.Load += new System.EventHandler(this.FormCompiler_Load);
            this.panelErrors.ResumeLayout(false);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelErrors;
        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.Splitter splitter;
        private System.Windows.Forms.RichTextBox richTextBoxCode;
        private System.Windows.Forms.Label labelErrors;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ListBox listBoxErrors;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowListErrors;
        private System.Windows.Forms.ToolStripDropDownButton toolStripFile;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemOpenFile;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSaveFile;
        private System.Windows.Forms.ToolStripDropDownButton toolStripComliling;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCompileWithoutExecute;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCompileAndExecute;
    }
}