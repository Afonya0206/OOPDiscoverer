﻿namespace OOPdiscoverer
{
    partial class TestingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelQuestion = new System.Windows.Forms.Panel();
            this.groupBoxQuestion = new System.Windows.Forms.GroupBox();
            this.labelAnswer = new System.Windows.Forms.Label();
            this.labelQuestion = new System.Windows.Forms.Label();
            this.radioButtonAnswer4 = new System.Windows.Forms.RadioButton();
            this.textBoxAnswer = new System.Windows.Forms.TextBox();
            this.radioButtonAnswer3 = new System.Windows.Forms.RadioButton();
            this.radioButtonAnswer2 = new System.Windows.Forms.RadioButton();
            this.radioButtonAnswer1 = new System.Windows.Forms.RadioButton();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.panelQuestion.SuspendLayout();
            this.groupBoxQuestion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelQuestion
            // 
            this.panelQuestion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelQuestion.Controls.Add(this.groupBoxQuestion);
            this.panelQuestion.Controls.Add(this.pictureBoxImage);
            this.panelQuestion.Controls.Add(this.buttonNext);
            this.panelQuestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQuestion.Location = new System.Drawing.Point(0, 0);
            this.panelQuestion.Margin = new System.Windows.Forms.Padding(4);
            this.panelQuestion.Name = "panelQuestion";
            this.panelQuestion.Size = new System.Drawing.Size(950, 466);
            this.panelQuestion.TabIndex = 4;
            // 
            // groupBoxQuestion
            // 
            this.groupBoxQuestion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBoxQuestion.Controls.Add(this.labelAnswer);
            this.groupBoxQuestion.Controls.Add(this.labelQuestion);
            this.groupBoxQuestion.Controls.Add(this.radioButtonAnswer4);
            this.groupBoxQuestion.Controls.Add(this.textBoxAnswer);
            this.groupBoxQuestion.Controls.Add(this.radioButtonAnswer3);
            this.groupBoxQuestion.Controls.Add(this.radioButtonAnswer2);
            this.groupBoxQuestion.Controls.Add(this.radioButtonAnswer1);
            this.groupBoxQuestion.Location = new System.Drawing.Point(12, 12);
            this.groupBoxQuestion.Name = "groupBoxQuestion";
            this.groupBoxQuestion.Size = new System.Drawing.Size(581, 379);
            this.groupBoxQuestion.TabIndex = 12;
            this.groupBoxQuestion.TabStop = false;
            this.groupBoxQuestion.Text = "groupBox1";
            // 
            // labelAnswer
            // 
            this.labelAnswer.AutoSize = true;
            this.labelAnswer.Location = new System.Drawing.Point(32, 139);
            this.labelAnswer.Name = "labelAnswer";
            this.labelAnswer.Size = new System.Drawing.Size(112, 16);
            this.labelAnswer.TabIndex = 13;
            this.labelAnswer.Text = "Введите ответ:";
            // 
            // labelQuestion
            // 
            this.labelQuestion.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelQuestion.Location = new System.Drawing.Point(32, 25);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(543, 107);
            this.labelQuestion.TabIndex = 12;
            this.labelQuestion.Text = "label1";
            this.labelQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButtonAnswer4
            // 
            this.radioButtonAnswer4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonAnswer4.Location = new System.Drawing.Point(35, 305);
            this.radioButtonAnswer4.Name = "radioButtonAnswer4";
            this.radioButtonAnswer4.Size = new System.Drawing.Size(540, 50);
            this.radioButtonAnswer4.TabIndex = 11;
            this.radioButtonAnswer4.Text = "radioButton4";
            this.radioButtonAnswer4.UseVisualStyleBackColor = true;
            this.radioButtonAnswer4.CheckedChanged += new System.EventHandler(this.radioButtonAnswer4_CheckedChanged);
            // 
            // textBoxAnswer
            // 
            this.textBoxAnswer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAnswer.Location = new System.Drawing.Point(35, 159);
            this.textBoxAnswer.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAnswer.Name = "textBoxAnswer";
            this.textBoxAnswer.Size = new System.Drawing.Size(285, 23);
            this.textBoxAnswer.TabIndex = 6;
            this.textBoxAnswer.TextChanged += new System.EventHandler(this.textBoxAnswer_TextChanged);
            // 
            // radioButtonAnswer3
            // 
            this.radioButtonAnswer3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonAnswer3.Location = new System.Drawing.Point(35, 248);
            this.radioButtonAnswer3.Name = "radioButtonAnswer3";
            this.radioButtonAnswer3.Size = new System.Drawing.Size(540, 50);
            this.radioButtonAnswer3.TabIndex = 10;
            this.radioButtonAnswer3.Text = "radioButton3";
            this.radioButtonAnswer3.UseVisualStyleBackColor = true;
            this.radioButtonAnswer3.CheckedChanged += new System.EventHandler(this.radioButtonAnswer3_CheckedChanged);
            // 
            // radioButtonAnswer2
            // 
            this.radioButtonAnswer2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonAnswer2.Location = new System.Drawing.Point(35, 191);
            this.radioButtonAnswer2.Name = "radioButtonAnswer2";
            this.radioButtonAnswer2.Size = new System.Drawing.Size(540, 50);
            this.radioButtonAnswer2.TabIndex = 9;
            this.radioButtonAnswer2.Text = "radioButton2";
            this.radioButtonAnswer2.UseVisualStyleBackColor = true;
            this.radioButtonAnswer2.CheckedChanged += new System.EventHandler(this.radioButtonAnswer2_CheckedChanged);
            // 
            // radioButtonAnswer1
            // 
            this.radioButtonAnswer1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButtonAnswer1.Location = new System.Drawing.Point(35, 135);
            this.radioButtonAnswer1.Name = "radioButtonAnswer1";
            this.radioButtonAnswer1.Size = new System.Drawing.Size(540, 50);
            this.radioButtonAnswer1.TabIndex = 8;
            this.radioButtonAnswer1.Text = "radioButton1";
            this.radioButtonAnswer1.UseVisualStyleBackColor = true;
            this.radioButtonAnswer1.CheckedChanged += new System.EventHandler(this.radioButtonAnswer1_CheckedChanged);
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxImage.ErrorImage = null;
            this.pictureBoxImage.Location = new System.Drawing.Point(599, 23);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(348, 344);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 7;
            this.pictureBoxImage.TabStop = false;
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNext.Location = new System.Drawing.Point(219, 398);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(4);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(179, 55);
            this.buttonNext.TabIndex = 5;
            this.buttonNext.Text = "Следующий вопрос";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // TestingForm
            // 
            this.AcceptButton = this.buttonNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 466);
            this.Controls.Add(this.panelQuestion);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TestingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тестирование";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestingForm_FormClosing);
            this.Load += new System.EventHandler(this.TestingForm_Load);
            this.panelQuestion.ResumeLayout(false);
            this.groupBoxQuestion.ResumeLayout(false);
            this.groupBoxQuestion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelQuestion;
        private System.Windows.Forms.TextBox textBoxAnswer;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.RadioButton radioButtonAnswer4;
        private System.Windows.Forms.RadioButton radioButtonAnswer3;
        private System.Windows.Forms.RadioButton radioButtonAnswer2;
        private System.Windows.Forms.RadioButton radioButtonAnswer1;
        private System.Windows.Forms.GroupBox groupBoxQuestion;
        private System.Windows.Forms.Label labelQuestion;
        private System.Windows.Forms.Label labelAnswer;
    }
}