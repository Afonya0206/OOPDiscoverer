﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// пространства имен для работы с базой данных
using System.Data;
using System.Data.OleDb;

namespace OOPdiscoverer
{
    class Test
    {
        // список вопросов в тесте
        List<Question> Questions;

        // количество вопросов в тесте
        int CountQuestions;

        // таблица для хранения вопросов
        DataTable TableQuestion;
        
        // таблица для хранения ответов
        DataTable TableAnswer;
        
        public Test(int count)
        {
            // инициализация списка
            Questions = new List<Question>();

            CountQuestions = count;

            // получение таблицы вопросов
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = DataBaseConnection.StringConnection;

            OleDbCommand command = new OleDbCommand();
            command.Connection = con;
            command.CommandType = CommandType.TableDirect;
            command.CommandText = "TestQuestion";

            OleDbDataAdapter adapter = new OleDbDataAdapter(command);
            TableQuestion = new DataTable();
            adapter.Fill(TableQuestion);

            // получение таблицы ответов
            command = new OleDbCommand();
            command.Connection = con;
            command.CommandType = CommandType.TableDirect;
            command.CommandText = "TestAnswer";

            adapter = new OleDbDataAdapter(command);
            TableAnswer = new DataTable();
            adapter.Fill(TableAnswer);

            // перемешивание вопросов
            MixQuestions();

            // заполнение списка значениями
            FillQuestions();
        }

        // индексатор для получения элемента из списка вопросов
        public Question this[int index]
        {
            get
            {
                if (index < 0 || index >= CountQuestions)
                    return null;
                return Questions[index];
            }
        }

        // свойство для получения максимального количества вопросов
        public int MaxCountQuestions
        {
            get
            {
                return TableQuestion.Rows.Count;
            }
        }

        // метод для перемешивания вопросов в случайном порядке
        private void MixQuestions()
        {
            // объект класса Random для генерации случайных чисел
            Random random = new Random();

            // формирование списка номеров вопросов
            List<int> list = new List<int>();
            for (int i = 0; i < TableQuestion.Rows.Count; i++)
            {
                list.Add(i);
            }

            // временная переменная для премещения двух элементов
            int temp;

            // переменная для хранения случайного числа
            int randomNumber;

            // перемешивание номеров элементов
            for (int i = 0; i < TableQuestion.Rows.Count; i++)
            {
                for (int j = 0; j < TableQuestion.Rows.Count; j++)
                {
                    randomNumber = random.Next(0, list.Count - 1);
                    temp = list[j];
                    list[j] = list[randomNumber];
                    list[randomNumber] = temp;
                }
            }

            // перезапись таблицы вопросов в сформированном порядке
            DataTable table = TableQuestion.Copy();
            TableQuestion.Clear();
            DataRow row;
            foreach (int i in list)
            {
                row = TableQuestion.NewRow();
                for (int j = 0; j < table.Columns.Count; j++)
                    row[j] = table.Rows[i][j];
                TableQuestion.Rows.Add(row);
            }
        }

        // метод для заполнения списка вопросов
        public void FillQuestions()
        {
            Question tempQuestion;
            DataTable tempTable;
            Answer tempAnswer;
            DataView dv = new DataView(TableAnswer);
            List<Answer> tempListAnswer;
            // каждой итерацией цикла заполняем один вопрос
            for (int i = 0; i < CountQuestions; i++)
            {
                tempListAnswer = new List<Answer>();

                // выбор из таблицы ответов тех ответов, которые относятся к данному вопросу
                dv.RowFilter = "NumberQuestion=" + TableQuestion.Rows[i]["Number"].ToString();
                tempTable = new DataTable();
                tempTable = dv.ToTable();
                for (int j = 0; j < tempTable.Rows.Count; j++)
                {
                    // создание ответа
                    if (tempTable.Rows[j]["IsRight"].ToString() == "1")
                        tempAnswer = new Answer(tempTable.Rows[j]["TextAnswer"].ToString(), true);
                    else
                        tempAnswer = new Answer(tempTable.Rows[j]["TextAnswer"].ToString(), false);

                    // добавление сфорированного ответа к списку ответов для текущего вопроса
                    tempListAnswer.Add(tempAnswer);
                }

                // формирование вопроса для добавления в список

                tempQuestion = new Question(TableQuestion.Rows[i]["Question"].ToString(), tempListAnswer, TableQuestion.Rows[i]["ImageQuestion"].ToString());

                // добавление сформированного вопроса к списку вопросов
                Questions.Add(tempQuestion);
            }

        }

        // метод для записи результатов прохождения тестирования
        public void WriteResult(string Surname, int CountQuestion, int CountRightQuestion)
        {
            string assessment = CalculateAssessment(CountQuestion, CountRightQuestion);
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = DataBaseConnection.StringConnection;

            OleDbCommand command = new OleDbCommand();
            command.Connection = con;

            command.CommandText = String.Format("Insert into Result(Surname, DateTesting, CountQuestion, CountRightQuestion, Assessment) " +
                "values('{0}','{1}', {2}, {3}, '{4}')", Surname, DateTime.Today, CountQuestion, CountRightQuestion, assessment);

            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }

        // метод для вычисления оценки
        public static string CalculateAssessment(int CountQuestion, int CountRightQuestion)
        {
            if (CountRightQuestion >= CountQuestion * 0.85)
                return "отлично";
            if (CountRightQuestion >= CountQuestion * 0.75)
                return "хорошо";
            if (CountRightQuestion >= CountQuestion * 0.5)   
                return "удовлетворительно";
            return "неудовлетворительно";
        }

        // 
        public static DataTable AllResults()
        {
            OleDbConnection con = new OleDbConnection();
            con.ConnectionString = DataBaseConnection.StringConnection;

            // получение таблицы результатов
            OleDbCommand command = new OleDbCommand();
            command.Connection = con;
            command.CommandType = CommandType.TableDirect;
            command.CommandText = "Result";

            OleDbDataAdapter adapter = new OleDbDataAdapter(command);
            DataTable result = new DataTable();
            adapter.Fill(result);
            return result;
        }
    }
}
