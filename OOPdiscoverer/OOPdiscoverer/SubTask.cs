﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTopic
{
    public class SubTask
    {
        string Question;
        string Answer;

        public string QUESTION
        {
            get
            {
                return Question;
            }
            set
            {
                Question = value;
            }
        }

        public string ANSWER
        {
            get
            {
                return Answer;
            }
            set
            {
                Answer = value;
            }
        }
    }
}
